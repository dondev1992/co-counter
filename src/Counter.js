import React, { useEffect, useState } from 'react';
import './Counter.css';

const Counter = () => {

    const [laps, lapsSet] = useState(0);
    const [lapHistory, lapHistorySet] = useState([]);
    const [recordedLaps, recordedLapsSet] = useState([]);


    const handleIncrement = () => {
        lapsSet(laps + 1)
    }

    const handleDecrement = () => {
        if (laps > 0) {
            lapsSet(laps - 1)
        }
    }

    const resetGame = () => {
        lapHistorySet([...lapHistory, laps]);
        lapsSet(0);
    }

    useEffect(() => {
        localStorage.setItem('history', JSON.stringify(lapHistory));
        recordedLapsSet(JSON.parse(localStorage.getItem('history')));
    }, [lapHistory]);

    return (
        <div>
            <h1>Counter Exercise</h1>
            <h2>Laps</h2>
            <h1 className='counter'>{laps}</h1>
            <div className='buttons'>
                <button
                    onClick={handleIncrement}
                >
                    Increment +
                </button>
                <button
                    onClick={handleDecrement}
                >
                    Decrement -
                </button>
            </div>
            <div>
                <button
                    onClick={resetGame}>Reset</button>
            </div>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Session</th>
                            <th># of Laps</th>
                        </tr>
                    </thead>
                    <tbody>
                        {recordedLaps.map((l, index) => (<tr key={index}>
                            <td>{index + 1}</td>
                            <td>{l}</td>
                        </tr>))}
                    </tbody>

                </table>

            </div>

        </div>

    )
}

export default Counter